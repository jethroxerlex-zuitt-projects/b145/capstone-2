const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');


//Registration

router.post('/register',(req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

//LOGIN
router.post('/login',(req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

//SET ADMIN

	router.put('/:userId/admin',auth.verify,(req,res) => {
		const data = {
			userId: req.params.userId,
			payload:  auth.decode(req.headers.authorization),
			setAdmin: req.body
		}
		userController.setAdmin(data).then(resultFromController => res.send(resultFromController))
	});


module.exports = router;