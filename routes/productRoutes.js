const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');

// Add a product

router.post('/', auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization)
	productController.addProduct(req.body, userData.isAdmin).then(resultFromController => res.send(resultFromController))
});

//retrieve all product

router.get('/all',auth.verify,(req,res) =>{

	const data = auth.decode(req.headers.authorization)

	productController.getAllProducts(data).then(resultFromController => res.send(resultFromController));
});

//retrieval of active product

router.get('/',(req,res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController))
})


// retrieval of specific product

router.get('/:productId', (req,res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
});



//update product

router.put('/:productId', auth.verify, (req,res) => {

	const data = {
		productId: req.params.productId,
		payload: auth.decode(req.headers.authorization),
		updatedProduct : req.body
	}

	productController.updateProduct(data).then(resultFromController => res.send(resultFromController))
});

//archiving a product

	router.put('/:productId/archive',auth.verify,(req,res) => {
		const data = {
			productId: req.params.productId,
			payload:  auth.decode(req.headers.authorization),
			archiveCourse: req.body
		}
		productController.archivedProduct(data).then(resultFromController => res.send(resultFromController))
	})


module.exports = router;

