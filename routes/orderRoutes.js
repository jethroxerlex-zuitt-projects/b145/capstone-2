const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController');
const auth = require('../auth');

router.post('/', (req, res) => {
    orderController.addOrder(req.body).then(resultFromController => res.send(resultFromController))
});

//retrieve all orders

router.get('/all',auth.verify,(req,res) =>{

    const data = auth.decode(req.headers.authorization)

   orderController.getAllOrder(data).then(resultFromController => res.send(resultFromController));
});


router.get('/:orderId', (req,res) => {
  orderController.getOrder(req.params).then(resultFromController => res.send(resultFromController))
});

module.exports = router;