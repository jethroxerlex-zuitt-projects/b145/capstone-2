const Product = require('../models/Product');
const User = require('../models/User')
const Order = require('../models/Order');

module.exports.addOrder = (reqBody) => {

    let newOrder = new Order({
        buyerName : reqBody.buyerName,
        totalAmount : reqBody.totalAmount,
        productBought : reqBody.productBought

    });

    return newOrder.save().then((order, err) => {

        if(err){
            return false;
        } else {
            return order;
        }
    })
}

module.exports.getAllOrder = async (user) => {
    if (user.isAdmin === true) {
        return Order.find({}).then(result => {
            return result
        })
    } else {
        return `${user.email} is not authorized`
    }
}


module.exports.getOrder = (reqParams) => {

  return Order.findById(reqParams.orderId).then(result => {
    return result
  })
}
