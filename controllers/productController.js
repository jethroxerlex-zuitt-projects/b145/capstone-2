const Product = require('../models/Product');
const User = require('../models/User');
const Order = require('../models/Order');

//Add a product

module.exports.addProduct = (reqBody,isAdmin) => {

if(isAdmin){

	let newProduct = new Product({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newProduct.save().then((product,err) => {

		if (err) {
			return false
		} else{
			return product;
		}
	})
}
	return Promise.resolve('You need to be an Admin to complete this!')
} 


//Retrieve all products

module.exports.getAllProducts = async (user) => {
	if (user.isAdmin === true) {
		return Product.find({}).then(result => {
			return result
		})
	} else {
		return `${user.email} is not authorized`
	}
}

//retrieval of active product

module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result
	})
}

// retrieval of a specific product

module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {
		return result
	})
}

//update a product

module.exports.updateProduct = (data) => {
	console.log(data)
	return Product.findById(data.productId).then((result,err) => {

		if (data.payload.isAdmin === true) {

			    result.name = data.updatedProduct.name
				result.description = data.updatedProduct.description
				result.price = data.updatedProduct.price
			
			return result.save().then((updatedProduct,err) => {
				if(err){
					return false
				} else{
					return updatedProduct
				}
			})
		} else {
			return false
		}
	})
}

//archive a product

module.exports.archivedProduct = async (data) => {
	console.log(data)
	return Product.findById(data.productId).then((result,err) => {
		if (data.payload.isAdmin === true) {

			 result.isActive = false
			 return result.save().then((archivedProduct,err) => {
			 	if (err) {
			 		return err
			 	} else {
			 		return archivedProduct
			 	}
			 })

		} else {
			return false
		}
	})
}