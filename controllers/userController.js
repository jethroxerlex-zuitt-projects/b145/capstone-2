const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth')
const Product = require('../models/Product');
const Order = require('../models/Order');

//REGISTRATION

module.exports.registerUser = (reqBody) =>{
	return User.find({email: reqBody.email}).then(result => {
		if (result.length > 0) {
			return `User is already taken!`
		} else {
			let newUser = new User({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				email: reqBody.email,
				mobileNo: reqBody.mobileNo,
				password: bcrypt.hashSync(reqBody.password,10)
			})
			return newUser.save().then((user,err) => {
				if (err) {
					return false
				} else {
					return user
				}
			})
		}
	})
}
	

	
//LOGIN

module.exports.loginUser = (reqBody) =>{

	return User.findOne({email : reqBody.email}).then(result => {
		if (result == null) {
			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}
			} else {
				return `Invalid Password`
			}
		}
	})
};

module.exports.setAdmin = async (data) => {
	console.log(data)
	return User.findById(data.userId).then((result,err) => {
		if (data.payload.isAdmin === true) {
			console.log(result)

			 result.isAdmin = data.setAdmin.isAdmin
			 return result.save().then((setAdmin,err) => {
			 	if (err) {
			 		return err
			 	} else {
			 		return setAdmin
			 	}
			 })

		} else {
			return false
		}
	})
};

