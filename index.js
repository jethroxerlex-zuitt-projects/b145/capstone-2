//[SECTION] DEPENDENCIES AND MODULES
	const express = require ('express');
	const mongoose = require ('mongoose');
	const cors = require('cors');
	const dotenv = require('dotenv');
	const userRoutes = require('./routes/userRoutes')
	const productRoutes = require('./routes/productRoutes')
	const orderRoutes = require('./routes/orderRoutes')

//[SECTION] ENVIRONMENT VARIABLE SETUP
	//configure the application in order for it to recognize and identify the necessary components needed to build the app successfully.
	dotenv.config();
	//extract the variables from the .env file.
	//verify the variable by displaying its value in the console. make sure to identify the origin of the component.
	// const secret = process.env.CONNECTION_STRING

//[SECTION] SERVER SETUP
	const app = express();
	const port = process.env.PORT || 4000;
	app.use(express.json());
	app.use(express.urlencoded({extended:true}));
	app.use(cors());
//Middlewares


//[SECTION] DATABASE CONNECT
//Connection String
	mongoose.connect("mongodb+srv://jramos:aketch24@wdc028-course-booking.il1nt.mongodb.net/ecommerce-api?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	});

	let db = mongoose.connection;
	db.on("error",console.error.bind(console,"There is an error with the connection"))
	db.once("open",() => console.log("Successfully connected to the database!"))

//[SECTION] SERVER ROUTES
	app.use('/users', userRoutes);
	app.use('/products', productRoutes);
	app.use('/orders', orderRoutes)

//[SECTION] SERVER RESPONSE
	app.listen(port,() => console.log(`Server running at port ${port}`));
